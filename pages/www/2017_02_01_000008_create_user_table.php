<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email', 45)->unique();
            $table->string('password', 50)->unique();
            $table->boolean('visible')->default(true);
            $table->timestampsTz();

            $table->softDeletesTz();
        });

        Schema::table('avatar', function (Blueprint $table) {
            $table->foreign('id_user')->references('id')->on('user')->onDelete('no action');
        });

        Schema::table('contacts', function (Blueprint $table) {
            $table->foreign('id_user')->references('id')->on('user')->onDelete('no action');
        });

        Schema::table('geo_contacts', function (Blueprint $table) {
            $table->foreign('id_user')->references('id')->on('user')->onDelete('no action');
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->foreign('id_user')->references('id')->on('user')->onDelete('no action');
        });

        Schema::table('subscriptions', function (Blueprint $table) {
            $table->foreign('id_subscriber')->references('id')->on('user')->onDelete('no action');
        });

        Schema::table('subscriptions', function (Blueprint $table) {
            $table->foreign('id_subscription')->references('id')->on('user')->onDelete('no action');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('avatar', function (Blueprint $table) {
            $table->dropForeign(['id_user']);
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->dropForeign(['id_user']);
        });

        Schema::table('contacts', function (Blueprint $table) {
            $table->dropForeign(['id_user']);
        });

        Schema::table('geo_contacts', function (Blueprint $table) {
            $table->dropForeign(['id_user']);
        });

        Schema::table('subscriptions', function (Blueprint $table) {
            $table->dropForeign(['id_subscriber']);
        });

        Schema::table('subscriptions', function (Blueprint $table) {
            $table->dropForeign(['id_subscription']);
        });

        Schema::drop('user');
    }
}