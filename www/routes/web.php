<?php

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::bind('user', function ($id){
    return \App\User::findOrFail($id);
});

Route::bind('post', function ($id){
    return \App\Post::withTrashed()->findOrFail($id);
});

Auth::routes();

Route::group(['middleware'=>'web'],function (){
    Route::auth();
    Route::get('/','HomeController@main');
    //To page home
    Route::get('/home', 'HomeController@index');
    //To search page
    Route::get('/home/{user}/search','NavbarController@search');
    //To news
    Route::get('/home/{user}/news','NavbarController@showNews');
    Route::get('/home/{user}/news/posts','NavbarController@showPosts');
    Route::get('/home/{user}/news/avatars','NavbarController@showAvatars');
    Route::get('/home/{user}/news/status','NavbarController@showStatus');
});

Route::group(['middleware'=>'auth'],function (){
    //To user's page
    Route::get('/home/{user}','UserController@show')->name('user.profile');

    Route::get('/home/{user}/#','PostController@showDeleted');

    //Uploading avatars
    Route::post('avatars','AvatarController@store');
    Route::get('/home/{user}/update-avatar/{avatar}','AvatarController@updateAvatar');
    Route::get('/home/{user}/delete-avatar/{avatar}','AvatarController@deleteAvatar');

    //Adding, updating and deleting posts
    Route::post('/home/{user}/new','PostController@addPost');
    Route::put('/post/{post}','PostController@updatePost');
    Route::get('/post/{post}/delete','PostController@deletePost');

    //Show, restore, delete deleted posts
    Route::get('/home/{user}/deleted-posts','PostController@showDeleted');
    Route::get('/post/{post}/restore','PostController@restorePost');
    Route::get('/post/{post}/force-delete','PostController@finalDelete');

    //Adding, updating, deleting comments
    Route::post('/home/{post}/new-comment','CommentController@leaveComment');
    Route::put('/comment/{comment}/update-comment','CommentController@updateComment');
    Route::get('/comment/{comment}/delete-comment','CommentController@deleteComment');

    //Adding, editing and deleting status
    Route::post('/user/{user}/status','StatusController@addStatus');
    Route::put('/status/{status}','StatusController@updateStatus');
    Route::get('/status/{status}/delete','StatusController@deleteStatus');

    //Showing another user's page
    Route::get('/user/{user}','UserController@show');

    //Showing and storing personal information
    Route::get('/home/{user}/settings','SettingsController@showSettings')->name('user.settings');
    Route::post('/home/{user}','SettingsController@settings');

    // Un/Subscribing methods
    Route::get('/user/{user}/subscribe','SubscriptionsController@subscribe');
    Route::get('/user/{user}/unsubscribe','SubscriptionsController@unSubscribe');

    //Subscribers and subscriptions pages
    Route::get('/user/{user}/subscribers','SubscriptionsController@showSubscribers');
    Route::get('/user/{user}/subscriptions','SubscriptionsController@showSubscriptions');

    //Like and dislike
    Route::get('user/{post}/like','LikesController@like');
    Route::get('user/{post}/dislike','LikesController@dislike');

    Route::get('/home/{user}/notifications','NotificationController@getIndex');

    //To search results
    Route::get('/home/{user}/search/byparam','SearchController@search');
    Route::get('/user/{user}/subscribers/search','SubscriptionsController@subscribers');
    Route::get('/user/{user}/subscriptions/search','SubscriptionsController@subscriptions');
});

Route::get('/login', 'Auth\LoginController@showLoginForm');
Route::post('/login', 'Auth\LoginController@login');

Route::get('/register', 'Auth\RegisterController@showRegistrationForm');
Route::post('/register', 'Auth\RegisterController@register');

Route::post('/logout', 'Auth\LoginController@logout');