@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <ul class="nav navbar-nav">

            </ul>
        </div>

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    You are logged in!
                </div>
                <div class="panel-body">
                    <a href="/home/{{ $user->id }}">To my page</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
