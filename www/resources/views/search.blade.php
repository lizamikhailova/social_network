@extends('layouts.app')
<?php
use Carbon\Carbon;
?>
@section('title')
    Search
@endsection

@section('content')
    <div class="container">
        <div class="col-md-8 col-sm-8 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading ">Search results</div>
                @if(isset($users))
                    @if($users->isEmpty())
                        <div class="panel-body">
                            <p>No results.</p>
                            <p>Try modifying search parameters.</p>
                        </div>
                    @endif
                    <div class="panel-body">
                        @foreach($users as $user)
                            <div class="media">
                                <div class="media-left media-top">
                                    <a href="/user/{{ $user->id }}">
                                        <img class="media-object" src="{{ Storage::disk('local')->url($user->avatars()->where('is_active', true)->first()->file_name) }}" width="50" height="50">
                                    </a>
                                </div>
                                @if($user->lastname()->first()==null)
                                    <div class="media-body">
                                        <p class="my-font"><a href="/user/{{ $user->id }}">{{ $user->name }}</a></p>
                                        @if(is_null($user->birthday)==false)
                                            <p class="my-font">{{ Carbon::now()->diffInYears(Carbon::createFromFormat('Y-m-d', $user->birthday)) }} years</p>
                                        @endif
                                    </div>
                                @else
                                    <div class="media-body">
                                        <p class="my-font"><a href="/user/{{ $user->id }}">{{ $user->name }} {{ $user->lastname()->first()->type_value }}</a></p>
                                        @if(is_null($user->birthday)==false)
                                            <p class="my-font">{{ Carbon::now()->diffInYears(Carbon::createFromFormat('Y-m-d', $user->birthday)) }} years</p>
                                        @endif
                                    </div>
                                @endif
                            </div>
                        @endforeach
                    </div>
                    @else
                    <div class="panel-body">
                        <p>No results yet.</p>
                        <p>Try modifying search parameters.</p>
                    </div>
                @endif
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading ">Searching parameters</div>
                    <div class="panel-body">
                        <form method="GET" action="/home/{{ Auth::user()->id }}/search/byparam">
                            <div class="form-group">
                                <label for="gender">By gender</label>
                                <select class="form-control" id="gender" name="gender">
                                    <option id="gender">Gender</option>
                                    <option id="male">Male</option>
                                    <option id="female">Female</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="age">By age</label>
                                <div class="form-inline">
                                    <select class="form-control" id="fromage" name="fromage">
                                        <option id="from">From</option>
                                        @for($i=0;$i<90;$i++)
                                            <option id="{{$i}}">{{ $i }}</option>
                                        @endfor
                                    </select>
                                    <select class="form-control" id="toage" name="toage">
                                        <option id="to">To</option>
                                        @for($i=0;$i<90;$i++)
                                            <option id="{{$i}}">{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="userName">By name</label>
                                <input type="text" class="form-control" id="name" name="name">
                            </div>
                            <div class="form-group">
                                <label for="last_name">By last name</label>
                                <input type="text" class="form-control" id="last_name" name="last_name">
                            </div>
                            <div class="panel-footer">
                                <button type="submit" class="btn btn-default btn-sm">Search</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection