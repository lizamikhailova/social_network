@extends('layouts.app')

@section('title')
    Personal information
@endsection

@section('content')
    <div class="container">
        <div class="well well-sm">
            <h4>Put checks near the fields that you wanna be seen</h4>
            <hr/>
                <form method="POST" action="/home/{{ $user->id }}">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                            <div class="input-group">
                                @if($last_name!=null)
                                    <span class="input-group-addon">
                                         @if($last_name->visible==true)
                                            <input type="checkbox" name="visible1" checked>
                                        @else
                                            <input type="checkbox" name="visible1">
                                        @endif
                                    </span>
                                    <input type="text" name="last_name" class="form-control" placeholder="Last name" id="lastName" value="{{ $last_name->type_value }}">
                                @else
                                    <span class="input-group-addon">
                                        <input type="checkbox" name="visible1">
                                    </span>
                                    <input type="text" name="last_name" class="form-control" placeholder="Last name" id="lastName" value="{{ $last_name }}">
                                @endif
                            </div>
                            <hr/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                            <div class="input-group">
                                @if($after_father!=null)
                                    <span class="input-group-addon">
                                        @if($last_name->visible==true)
                                            <input type="checkbox" name="visible2" checked>
                                        @else
                                            <input type="checkbox" name="visible2">
                                        @endif
                                    </span>
                                    <input type="text" name="after_father" class="form-control" placeholder="Name after father" id="afterFather" value="{{ $after_father->type_value }}">
                                @else
                                    <span class="input-group-addon">
                                        <input type="checkbox" name="visible2">
                                    </span>
                                    <input type="text" name="after_father" class="form-control" placeholder="Name after father" id="afterFather" value="{{ $after_father }}">
                                @endif
                            </div>
                            <hr/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                            <div class="input-group">
                                @if($phone_number!=null)
                                    <span class="input-group-addon">
                                        @if($phone_number->visible==true)
                                            <input type="checkbox" name="visible4" checked>
                                        @else
                                            <input type="checkbox" name="visible4">
                                        @endif
                                    </span>
                                    <input type="tel" name="phone_number" class="form-control" placeholder="Phone number" id="phoneNumber" value="{{ $phone_number->type_value }}">
                                @else
                                    <span class="input-group-addon">
                                        <input type="checkbox" name="visible4">
                                    </span>
                                    <input type="tel" name="phone_number" class="form-control" placeholder="Phone number" id="phoneNumber" value="{{ $phone_number }}">
                                @endif
                            </div>
                            <hr/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                            <div class="input-group">
                                @if($skype!=null)
                                    <span class="input-group-addon">
                                        @if($skype->visible==true)
                                            <input type="checkbox" name="visible3" checked>
                                        @else
                                            <input type="checkbox" name="visible3">
                                        @endif
                                    </span>
                                    <input type="text" name="skype" class="form-control" placeholder="Skype" id="skype" value="{{ $skype->type_value }}">
                                @else
                                    <span class="input-group-addon">
                                        <input type="checkbox" name="visible3">
                                    </span>
                                    <input type="text" name="skype" class="form-control" placeholder="Skype" id="skype" value="{{ $skype }}">
                                @endif
                            </div>
                            <hr/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                            Birthday:
                            <div class="input-group">
                                @if($bday!=null)
                                    <span class="input-group-addon">
                                        @if($bdayvis==true)
                                            <input type="checkbox" name="bdayvis" checked>
                                        @else
                                            <input type="checkbox" name="bdayvis">
                                        @endif
                                    </span>
                                    <input class="form-control" type="date" name="bday" value="{{ $bday }}">
                                @else
                                    <span class="input-group-addon">
                                        @if($bdayvis==true)
                                            <input type="checkbox" name="bdayvis" checked>
                                        @else
                                            <input type="checkbox" name="bdayvis">
                                        @endif
                                    </span>
                                    <input class="form-control" type="date" name="bday" required>
                                @endif
                            </div>
                            <hr/>
                        </div>
                    </div>

                    @if($gender==null)
                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1">
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="gender" value="male" required> Male
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="gender" value="female" required> Female
                                    </label>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1">
                                <div class="radio">
                                    @if($gender->type_value=="male")
                                        <label class="radio-inline">
                                            <input type="radio" name="gender" value="male" checked> Male
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="gender" value="female"> Female
                                        </label>
                                    @else
                                        <label class="radio-inline">
                                            <input type="radio" name="gender" value="male"> Male
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="gender" value="female" checked> Female
                                        </label>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                            <div class="checkbox">
                                <label>
                                    @if($agreement==true)
                                        <input type="checkbox" name="agreement" value="agreement" checked>
                                    @else
                                        <input type="checkbox" name="agreement" value="agreement">
                                    @endif
                                    Do you allow other users post on your page?
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                            <div class="checkbox">
                                <label>
                                    @if($visible==true)
                                        <input type="checkbox" name="netVisibility" value="netVisibility" checked>
                                    @else
                                        <input type="checkbox" name="netVisibility" value="netVisibility">
                                    @endif
                                    Change visibility of your page in the network
                                </label>
                            </div>
                            <hr/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                            <ul class="list-inline">
                                <li><button type="submit" class="btn btn-primary">Save</button></li>
                                <li><a href="/home/{{ $user->id }}">Cancel</a></li>
                            </ul>
                        </div>
                    </div>
                </form>
        </div>
    </div>
@endsection