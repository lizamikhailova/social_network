<?php
use Carbon\Carbon;
?>
@extends('layouts.app')

@section('title')
    Subscribers
@endsection

@section('content')
    <div class="container">
        <div class="col-lg-8 col-lg-offset-0">
            @if($subscribers->isEmpty())
                <div class="panel panel-default">
                    <div class="panel-heading"><strong>Subscribers</strong></div>
                        <div class="panel-body">
                            <p>You've got no subscribers yet</p>
                        </div>
                </div>
            @else
                <div class="panel panel-default">
                    <div class="panel-heading"><strong>Subscribers</strong></div>
                    <div class="panel-body">
                        @foreach($subscribers as $subscriber)
                            <div class="media">
                                <div class="media-left media-middle">
                                    <a href="/user/{{ $subscriber->id }}">
                                        <img class="media-object" src="{{ Storage::disk('local')->url($subscriber->avatars()->where('is_active', true)->first()->file_name) }}" width="50" height="50">
                                    </a>
                                </div>
                                @if(is_null($subscriber->lastname()->first()))
                                    <div class="media-body">
                                        <p class="my-font"><a href="/user/{{ $subscriber->id }}"> {{ $subscriber->name }} </a></p>
                                        @if(is_null($subscriber->birthday)==false)
                                            <p class="my-font">{{ Carbon::now()->diffInYears(Carbon::createFromFormat('Y-m-d', $subscriber->birthday)) }} years</p>
                                        @endif
                                    </div>
                                @else
                                    <div class="media-body">
                                        <p class="my-font"><a href="/user/{{ $subscriber->id }}">{{ $subscriber->name }} {{ $subscriber->lastname()->first()->type_value }}</a></p>
                                        @if(is_null($subscriber->birthday)==false)
                                            <p class="my-font">{{ Carbon::now()->diffInYears(Carbon::createFromFormat('Y-m-d', $subscriber->birthday)) }} years</p>
                                        @endif
                                    </div>
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
        </div>
        <div class="col-md-4 col-sm-4 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading ">Searching parameters</div>
                <div class="panel-body">
                    <form method="GET" action="/user/{{ $user->id }}/subscribers/search">
                        <div class="form-group">
                            <label for="gender">By gender</label>
                            <select class="form-control" id="gender" name="gender">
                                <option id="gender">Gender</option>
                                <option id="male">Male</option>
                                <option id="female">Female</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="age">By age</label>
                            <div class="form-inline">
                                <select class="form-control" id="fromage" name="fromage">
                                    <option id="from">From</option>
                                    @for($i=0;$i<90;$i++)
                                        <option id="{{$i}}">{{ $i }}</option>
                                    @endfor
                                </select>
                                <select class="form-control" id="toage" name="toage">
                                    <option id="to">To</option>
                                    @for($i=0;$i<90;$i++)
                                        <option id="{{$i}}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="userName">By name</label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                        <div class="form-group">
                            <label for="last_name">By last name</label>
                            <input type="text" class="form-control" id="last_name" name="last_name">
                        </div>
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-default btn-sm">Search</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection