@extends('layouts.app')

<?php
use Carbon\Carbon;
?>
@section('title')
    Notifications
@endsection

@section('content')
    <div class="container">
        <div class="col-lg-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Notifications</div>
                <div class="panel-body">
                    @if(Auth::user()->notifications()->get()->isEmpty())
                        <p>No notifications</p>
                    @else
                        @foreach(Auth::user()->notifications()->get() as $note)
                            <?php
                            dd($note->data);
                            ?>
                            {{--@if(is_null($note -> data["post"])==false)--}}
                                {{--<div class="media">--}}
                                    {{--<div class="media-body">--}}
                                        {{--{{ $note -> data["post"] }}--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--@elseif(is_null($note -> data["comment"])==false)--}}
                                {{--<div class="media">--}}
                                    {{--<div class="media-body">--}}
                                        {{--{{ $note -> data["comment"] }}--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--@endif--}}
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection