@if(Auth::user()->id!=$user->id)
    <div class="well well-sm my-well">
        @if($subscript->isEmpty())
            <p class="my-font">You haven't subscribed</p>
            <a href="/user/{{ $user->id }}/subscribe"><strong>Subscribe</strong></a>
        @else
            <p class="my-font">You have subscribed</p>
            <a href="/user/{{ $user->id }}/unsubscribe"><strong>Unsubscribe</strong></a>
        @endif
    </div>
@endif
<div class="well well-sm my-well">
    <p><a href="/user/{{ $user->id }}/subscribers">Subscribers</a></p>
    @if($subscribers->isEmpty())
        <p class="my-font">No subscribers</p>
    @else
        @foreach ($subscribers->chunk(3) as $chunk)
            <div class="row">
                @foreach ($chunk as $friend)
                    <div class="col-md-4">
                        <a href="/user/{{ $friend->id }}">
                            <img src="{{ Storage::disk('local')->url($friend->avatars()->where('is_active',true)->first()->file_name) }}" width="50px" height="50px">
                            <p class="my-font">{{ $friend->name }}</p>
                        </a>
                    </div>
                @endforeach
            </div>
        @endforeach
    @endif

    <p><a href="/user/{{ $user->id }}/subscriptions">Subscriptions</a></p>
    @if($subscriptions->isEmpty())
        <p class="my-font">No subscriptions</p>
    @else
        @foreach ($subscriptions->chunk(3) as $chunk)
            <div class="row">
                @foreach ($chunk as $friend)
                    <div class="col-md-4">
                        <a href="/user/{{ $friend->id }}">
                            <img src="{{ Storage::disk('local')->url($friend->avatars()->where('is_active',true)->first()->file_name) }}" width="50px" height="50px">
                            <p class="my-font">{{ $friend->name }}</p>
                        </a>
                    </div>
                @endforeach
            </div>
        @endforeach
    @endif
</div>