<div class="well well-sm my-well">
    <p><u>Personal information</u></p>
    <dl class="dl-horizontal">
        @if(Auth::user()->id!=$user->id)
            @if($last_name!=null)
                @if($last_name->visible==true)
                    <dt>Last name:</dt>
                    <dd>{{ $last_name->type_value }}</dd>
                @endif
            @endif

            @if($after_father!=null)
                @if($after_father->visible==true)
                    <dt>Name after father:</dt>
                    <dd>{{ $after_father->type_value }}</dd>
                @endif
            @endif

            @if($phone_number!=null)
                @if($phone_number->visible==true)
                    <dt>Phone number:</dt>
                    <dd>{{ $phone_number->type_value }}</dd>
                @endif
            @endif

            @if($skype!=null)
                @if($skype->visible==true)
                    <dt>Skype:</dt>
                    <dd>{{ $skype->type_value }}</dd>
                @endif
            @endif
            @if($bday!=null)
                @if($bdayvis==true)
                    <dt>Birth date:</dt>
                    <dd>{{ $bday }}</dd>
                @endif
            @endif
        @else
            @if($last_name!=null)
                <dt>Last name:</dt>
                <dd>{{ $last_name->type_value }}</dd>
            @endif

            @if($after_father!=null)
                <dt>Name after father:</dt>
                <dd>{{ $after_father->type_value }}</dd>
            @endif

            @if($phone_number!=null)
                <dt>Phone number:</dt>
                <dd>{{ $phone_number->type_value }}</dd>
            @endif

            @if($skype!=null)
                <dt>Last name:</dt>
                <dd>{{ $skype->type_value }}</dd>
            @endif

            @if($bday!=null)
                <dt>Birth date:</dt>
                <dd>{{ $bday }}</dd>
            @endif
        @endif
    </dl>
</div>