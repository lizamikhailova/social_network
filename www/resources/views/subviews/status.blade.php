@if(Auth::user()->id!=$user->id)
    @if($status==null)
        <h4 class="status" id="status"><span class="label label-info">Status:</span>  No status</h4>
    @else
        <h4 class="status"><span class="label label-info">Status:</span>  {{ $status->status }}</h4>
    @endif
@else
    @if($status==null)
        <div class="btn-group">
            <h4 class="status">
                <span class="label label-info label-button">Status:</span>
                <ul class="dropdown-menu ul-toggle">
                    <li><a data-toggle="modal" data-target="#addStatusModal">Add</a></li>
                </ul>
                No status
            </h4>
        </div>
        <div class="modal fade" id="addStatusModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-sm modal-dialog" role="form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Add Status</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form" role="form" method="POST" action="/user/{{ $user->id }}/status">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="text" class="form-control" name="status" value="" required>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-info btn-md" aria-label="Center">
                                    <span class="glyphicon glyphicon-ok"></span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="btn-group">
            <h4 class="status">
                <span class="label label-info label-button">Status:</span>
            <ul class="dropdown-menu ul-toggle">
                <li><a data-toggle="modal" data-target="#editStatusModal-{{ $status->id }}">Edit</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="/status/{{ $status->id }}/delete">Delete</a></li>
            </ul>
                {{ $status->status }}
            </h4>
        </div>
        <div class="modal fade" id="editStatusModal-{{ $status->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm" role="form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Edit Status</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form" role="form" method="POST" action="/status/{{ $status->id }}">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="form-group">
                                <input type="text" class="form-control" name="status" value="{{ $status->status }}" required>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-info btn-md" aria-label="Center">
                                    <span class="glyphicon glyphicon-ok"></span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endif