<style>
    .my-dropdown {
        position: absolute;
        right: 20px;
        top: 40px;
    }
    .my-menu {
        left: -140px;
    }
</style>
<div class="panel panel-default">
    @if($posts->isEmpty())
        <div class="panel-heading"><strong>You have no posts yet</strong>
            @if(\Auth::user()->id==$user->id)
                <a class="filter" href="/home/{{ $user->id }}/deleted-posts">
                    Deleted posts
                </a>
            @endif
        </div>
    @else
        <div class="panel-heading"><strong>All your Posts</strong>
            @if(\Auth::user()->id==$user->id)
                <a class="filter" href="/home/{{ $user->id }}/deleted-posts">
                    Deleted posts
                </a>
            @endif
        </div>
        <div class="panel-body">
            @foreach($posts as $post)
                <div class="well specwell">
                    <div class="media">
                        <div class="media-left media-top">
                            <a href="/user/{{ $post->id_user }}">
                                <img class="media-object" src="{{ Storage::disk('local')->url($post->user->avatars()->where('is_active',true)->first()->file_name) }}" width="50" height="50">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading"><a href="/user/{{ $post->id_user }}"><cite title="Author">{{ $post->user->name }}</cite></a></h4>
                            <h4 class="my-font">{{ $post->post }}</h4>
                            @if($post->id_user==Auth::user()->id)
                                <div class="my-dropdown dropdown" role="menu">
                                    <button type="button" class="btn btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="glyphicon glyphicon-option-vertical"></span>
                                    </button>
                                    <ul class="dropdown-menu my-menu">
                                        <li><a data-toggle="modal" data-target="#editModal-{{ $post->id }}">Edit</a></li>
                                        <li><a href="/post/{{ $post->id }}/delete">Delete</a></li>
                                    </ul>
                                </div>
                            @elseif(Auth::user()->id==$post->id_profile)
                                <div class="my-dropdown dropdown" role="menu">
                                    <button type="button" class="btn btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="glyphicon glyphicon-option-vertical"></span>
                                    </button>
                                    <ul class="my-menu dropdown-menu">
                                        <li><a href="/post/{{ $post->id }}/delete">Delete</a></li>
                                    </ul>
                                </div>
                            @endif
                            <p class="my-font">{{ $post['attributes']["created_at"] }}</p>
                            <div class="modal fade" id="editModal-{{ $post->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="form">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">Edit post</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form class="form" role="form" method="POST" action="/post/{{ $post->id }}">
                                                {{ csrf_field() }}
                                                {{ method_field('PUT') }}
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="post" value="{{ $post->post }}" required>
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-info btn-md" aria-label="Center">
                                                        <span class="glyphicon glyphicon-ok"></span>
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <ul class="list-inline">
                                <?php
                                    $me = Auth::user();
                                    $like = $post->likes()->where('id_user',Auth::user()->id)->get();
                                    ?>
                                <li>
                                    @if($like->isEmpty())
                                        <a class="likes" href="/user/{{ $post->id }}/like">Likes <span class="badge">{{ $post->likes()->get()->count() }}</span></a>
                                    @else
                                        <a class="likes" href="/user/{{ $post->id }}/dislike">I like it <span class="badge">{{ $post->likes()->get()->count() }}</span></a>
                                    @endif
                                </li>
                                <li><p class="text-primary"><u>Comments</u></p></li>
                            </ul>
                            @if(is_null($post->comments()->get())==false)
                                @foreach($post->comments()->get() as $comment)
                                    <blockquote>
                                        <ul class="list-inline">
                                            <li><p class="my-font">{{ $comment->comment}}</p></li>
                                            <li class="filter">
                                                @if($comment->id_user==Auth::user()->id)
                                                    <div class="dropdown" role="menu">
                                                        <button type="button" class="btn btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span class="glyphicon glyphicon-option-vertical"></span>
                                                        </button>
                                                        <ul class="dropdown-menu my-menu">
                                                            <li><a data-toggle="modal" data-target="#editCommentModal-{{ $comment->id }}">Edit</a></li>
                                                            <li><a href="/comment/{{ $comment->id }}/delete-comment">Delete</a></li>
                                                        </ul>
                                                    </div>
                                                @elseif(Auth::user()->id==$comment->post->id_profile)
                                                    <div class="dropdown" role="menu">
                                                        <button type="button" class="btn btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span class="glyphicon glyphicon-option-vertical"></span>
                                                        </button>
                                                        <ul class="dropdown-menu my-menu">
                                                            <li><a href="/comment/{{ $comment->id }}/delete-comment">Delete</a></li>
                                                        </ul>
                                                    </div>
                                                @endif
                                            </li>
                                        </ul>
                                        <footer class="bblockquote-footer">
                                            <a class="my-font" href="/user/{{ $comment->id_user }}">{{ $comment->user->name}}</a>
                                            {{ $comment['attributes']['created_at'] }}
                                        </footer>
                                        <div class="modal fade" id="editCommentModal-{{ $comment->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="form">
                                                <div class="modal-content modal-md">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <h4 class="modal-title" id="myModalLabel">Edit comment</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form class="form" role="form" method="POST" action="/comment/{{ $comment->id }}/update-comment">
                                                            {{ csrf_field() }}
                                                            {{ method_field('PUT') }}
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="comment" value="{{ $comment->comment }}" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <button type="submit" class="btn btn-info btn-md" aria-label="Center">
                                                                    <span class="glyphicon glyphicon-ok"></span>
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </blockquote>
                                @endforeach
                            @endif
                            <h4 class="info text-primary leave-comment my-font">Leave a comment</h4>
                                <form class="form addcomment" role="form" method="POST" action="/home/{{ $post->id }}/new-comment">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <input id="comment" type="text" class="form-control" name="comment" required>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-sm" aria-label="Center">
                                            Add
                                        </button>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
</div>