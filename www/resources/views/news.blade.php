@extends('layouts.app')

@section('title')
    News
@endsection

@section('content')

    <div class="col-lg-8 col-lg-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <ul class="list-inline">
                    <li><strong>News</strong></li>
                    <li class="dropdown filter">
                        <button type="button" class="btn btn-default btn-md dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Filter
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/home/{{ $user->id }}/news">All</a></li>
                            <li><a href="/home/{{ $user->id }}/news/posts">Posts</a></li>
                            <li><a href="/home/{{ $user->id }}/news/avatars">Avatars</a></li>
                            <li><a href="/home/{{ $user->id }}/news/status">Status</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
                <div class="panel-body">
                @if($news->isEmpty())
                    <p><strong>You've got no news :) </strong></p>
                @else
                    @if($type=="all")
                        <ul class="list-group">
                            @foreach($news as $each)
                                @if($each["type"]=='post')
                                    <li class="list-group-item">
                                        <div class="media-left media-middle">
                                            <a href="/user/{{ $each["user_id"] }}">
                                                <img class="media-object" src="{{ Storage::disk('local')->url($each["file_name"]) }}" width="50" height="50">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h4><a href="/user/{{ $each["user_id"] }}">{{ $each["user_name"] }}</a> added new post</h4>
                                            <p>{{ $each["property"] }}</p>
                                            <p>{{ $each["updated_at"] }}</p>
                                        </div>
                                    </li>
                                @elseif($each["type"]=='status')
                                    <li class="list-group-item">
                                        <div class="media-left media-middle">
                                            <a href="/user/{{ $each["user_id"] }}">
                                                <img class="media-object" src="{{ Storage::disk('local')->url($each["file_name"]) }}" width="50" height="50">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h4><a href="/user/{{ $each["user_id"] }}">{{ $each["user_name"] }}</a> added new status</h4>
                                            <p>{{ $each["property"] }}</p>
                                            <p>{{ $each["updated_at"] }}</p>
                                        </div>
                                    </li>
                                @else
                                    <li class="list-group-item">
                                        <h4><a href="/user/{{ $each["user_id"] }}">{{ $each["user_name"] }}</a> updated avatar</h4>
                                        <div class="thumbnail">
                                            <img class="media-object" src="{{ Storage::disk('local')->url($each["property"]) }}" class="img-responsive">
                                        </div>
                                        <p>{{ $each["updated_at"] }}</p>
                                    </li>
                                @endif
                            @endforeach
                        </ul>

                        @elseif($type=="posts")
                            <ul class="list-group">
                                @foreach($news as $post)
                                    <li class="list-group-item">
                                        <div class="media-left media-top">
                                            <a href="/user/{{ $post->id_user }}">
                                                <img class="media-object" src="{{ Storage::disk('local')->url($post->user->avatars()->where('is_active', true)->first()->file_name) }}" width="50" height="50">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h4><a href="/user/{{ $post->id_user }}">{{ $post->user->name }}</a> added new post</h4>
                                            <p>{{ $post->post }}</p>
                                            <p>{{ $post['attributes']["created_at"] }}</p>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">Comments</div>
                                                <div class="panel-body">
                                                    @if(is_null($post->comments()->get())==false)
                                                        @foreach($post->comments()->get() as $comment)
                                                            <blockquote>
                                                                <p>{{ $comment->comment}}</p>
                                                                <footer class="bblockquote-footer">
                                                                    <a href="/user/{{ $comment->id_user }}">{{ $comment->user->name}}</a>
                                                                    {{ $comment['attributes']['created_at'] }}
                                                                </footer>
                                                            </blockquote>
                                                        @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        @elseif($type=="avatars")
                            <ul class="list-group">
                                @foreach($news as $avatars)
                                    <li class="list-group-item">
                                        <h4><a href="/user/{{ $avatars->id_user }}">{{ $avatars->user->name }}</a> updated avatar</h4>
                                        <div class="thumbnail">
                                            <img class="media-object" src="{{ Storage::disk('local')->url($avatars->file_name) }}" class="img-responsive">
                                        </div>
                                        <p>{{ $avatars['attributes']["created_at"] }}</p>
                                    </li>
                                @endforeach
                            </ul>
                        @elseif($type=="status")
                            <ul class="list-group">
                                @foreach($news as $status)
                                    <li class="list-group-item">
                                        <div class="media-left media-top">
                                            <a href="/user/{{ $status->id_user }}">
                                                <img class="media-object" src="{{ Storage::disk('local')->url($status->user->avatars()->where('is_active', true)->first()->file_name) }}" width="50" height="50">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h4><a href="/user/{{ $status->id_user }}">{{ $status->user->name }}</a> updated status</h4>
                                            <p>{{ $status->status }}</p>
                                            <p>{{ $status['attributes']["created_at"] }}</p>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                @endif
            </div>
        </div>
    </div>
@endsection