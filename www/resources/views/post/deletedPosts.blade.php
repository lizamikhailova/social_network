@extends('layouts.app')

@section('title')
    Deleted posts
@endsection

@section('content')
    <div class="container">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="panel panel-info">
                @if($posts->isEmpty())
                    <div class="panel-heading"><strong>No deleted posts</strong></div>
                @else
                    <div class="panel-heading"><strong>Deleted posts</strong></div>
                    <div class="panel-body">
                        @foreach($posts as $post)
                            <blockquote class="blockquote">
                                <h4 id="myPost" class="mb-0">{{ $post->post }}</h4>
                                <footer class="blockquote-footer"> {{ $post["attributes"]["created_at"] }}</footer>
                                <div class="dropdown" role="menu">
                                    <button type="button" class="btn btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="glyphicon glyphicon-option-vertical"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="/post/{{ $post->id }}/restore">Restore</a></li>
                                        <li><a href="/post/{{ $post->id }}/force-delete">Delete</a></li>
                                    </ul>
                                </div>
                            </blockquote>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection