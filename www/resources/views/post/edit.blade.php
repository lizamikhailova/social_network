@extends('layouts.app')
@section('content')
    <div class="container">
    <h1>Edit the post</h1>
    <form class="form" role="form" method="POST" action="/post/{{ $post->id }}/edit">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <div class="form-group">
            <input id="post" type="text" class="form-control" name="post" value="{{$post->post}}" required>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-default btn-md" aria-label="Center"><span class="glyphicon glyphicon-ok"></span></button>
        </div>
    </form>
    </div>
@endsection