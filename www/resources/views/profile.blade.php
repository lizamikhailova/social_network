@extends('layouts.app')

@section('title')
    Profile
@endsection

@section('content')
    <div class="left-block">
        <div class="col-lg-7 col-lg-offset-2">
            <div class="page-header">
                <h2 class="name">{{ $user->name }}</h2>
            </div>

            <div class="thumbnail">
                <img class="image img-button" src="{{ Storage::disk('local')->url($avatar->file_name) }}">
                @if(Auth::user()->id==$user->id)
                    <div class="well well-toggle">
                        <form class="form-inline" method="POST" action="/avatars" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-inline">
                                <div class="form-group">
                                    <input type="file" name="avatar">
                                </div>
                                <button type="submit" class="btn btn-default btn-sm">Add</button>
                            </div>
                        </form>
                    </div>
                @endif
                {{--<button class="btn btn-default btn-sm" data-target="#cropAvatarModal" data-toggle="modal">Crop</button>--}}
                {{--<div class="modal fade" id="cropAvatarModal" aria-labelledby="modalLabel" role="dialog" tabindex="-1">--}}
                    {{--<div class="modal-dialog" role="document">--}}
                        {{--<div class="modal-content">--}}
                            {{--<div class="modal-header">--}}
                                {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>--}}
                                {{--<h5 class="modal-title" id="modalLabel">Crop the image</h5>--}}
                            {{--</div>--}}
                            {{--<div class="modal-body">--}}
                                {{--<div>--}}
                                    {{--<img id="image" src="{{ Storage::disk('local')->url($avatar->file_name) }}" alt="Picture">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="modal-footer">--}}
                                {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
            <p class="text-right"><a data-toggle="modal" data-target="#allAvatarsModal">All avatars</a></p>
            <div class="modal fade" id="allAvatarsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-lg modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">{{ $user->name }}'s avatars</h4>
                        </div>
                        <div class="modal-body">
                            @foreach ($user->avatars()->get()->chunk(3) as $chunk)
                                <div class="row">
                                    @foreach ($chunk as $avatar)
                                        <div class="col-md-4">
                                            <a class="toggleDelete" href="/home/{{ $user->id }}/update-avatar/{{ $avatar->id }}">
                                                <img name="avatar" src="{{ Storage::disk('local')->url($avatar->file_name) }}" class="img-responsive">
                                            </a>
                                            <a class="deleteAvatar" href="/home/{{ $user->id }}/delete-avatar/{{ $avatar->id }}">
                                                <span aria-hidden="true">&times;</span>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
                @include('subviews.info')
                @include('subviews.friends')
        </div>
    </div>
    <div class="right-block">
        <div class="col-lg-10">

            @include('subviews.status')

            <hr/>
            <div class="well">
                @if($user->id==Auth::user()->id)
                    <h4> What's on your mind? </h4>
                    <form class="form" role="form" method="POST" action="/home/{{ $user->id }}/new">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input id="post" type="text" class="form-control" name="post" required>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-info btn-sm" aria-label="Center">Add</button>
                        </div>
                    </form>
                @else
                    @if($agreement==true)
                        <h4>You can suggest your post</h4>
                        <form class="form" role="form" method="POST" action="/home/{{ $user->id }}/new">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input id="post" type="text" class="form-control" name="post" required>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-info btn-sm" aria-label="Center">Add</button>
                            </div>
                        </form>
                        @endif
                @endif
            </div>
            @include('subviews.postpanel')
        </div>
    </div>
    <script>
        $(document).ready(main);
        $(function () {
            var $image = $('#image');
            var cropBoxData;
            var canvasData;
            $('#cropAvatarModal').on('shown.bs.modal', function () {
                $image.cropper({
                    autoCropArea: 0.5,
                    ready: function () {
                        $image.cropper('setCanvasData', canvasData);
                        $image.cropper('setCropBoxData', cropBoxData);
                    }
                });
            }).on('hidden.bs.modal', function () {
                cropBoxData = $image.cropper('getCropBoxData');
                canvasData = $image.cropper('getCanvasData');
                $image.cropper('destroy');
            });
        });
        function main(){
            $('.well-toggle').hide();
            $('.img-button').on('click',function(){
                $(this).next().slideToggle(400);
                $(this).toggleClass('active');
            });

            $('.deleteAvatar').hide();
            $('.toggleDelete').on('mouseover',function() {
                $(this).next().toggle();
                $(this).toggleClass('active');
            });
            $('.deleteAvatar').on('mouseleave',function() {
                $(this).toggle();
            });

            $('.ul-toggle').hide();
            $('.label-button').on('mouseover',function(){
                $(this).next().slideToggle(400);
                $(this).toggleClass('active');
            });
            $('.ul-toggle').on('mouseleave',function () {
                $(this).slideToggle(400);
            });

            $('.addcomment').hide();
            $('.leave-comment').on('click',function () {
                $(this).next().toggle();
                $(this).toggleClass('active');
            });
        }
    </script>
@endsection