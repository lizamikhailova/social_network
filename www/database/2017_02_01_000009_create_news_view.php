<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use DB;

class CreateNewsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW news AS
                SELECT user.id AS user_id,
                    user.name AS user_name,
                    avatar.id AS id_prop,
                    avatar.file_name AS property,
                    avatar.updated_at AS updated_at
                FROM (user
                JOIN avatar ON ((avatar.id_user = user.id)))
                
               UNION
               
                SELECT user.id AS user_id,
                user.name AS user_name,
                posts.id AS id_prop,
                posts.post AS property,
                posts.updated_at AS updated_at
               FROM (user
                 JOIN posts ON (((posts.id_user = user.id) AND (posts.id_profile = user.id))))
                 
               UNION
               
                 SELECT user.id AS user_id,
                    user.name AS user_name,
                    status.id AS id_prop,
                    status.status AS property,
                    status.updated_at AS updated_at
                   FROM (user
                     JOIN status ON ((status.id_user = user.id)))
               ORDER BY updated_at DESC;
           "
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW news_view');
    }
}
