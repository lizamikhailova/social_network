<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_subscriber')->index();
            $table->integer('id_subscription')->index();

            $table->unique(['id_subscriber', 'id_subscription']);

            $table->foreign('id_subscriber')->references('id')->on('user')->onDelete('cascade');
            $table->foreign('id_subscription')->references('id')->on('user')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->dropForeign(['id_subscriber']);
        });
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->dropForeign(['id_subscription']);
        });

        Schema::drop('subscriptions');
    }
}