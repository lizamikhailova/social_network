<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use DB;

class CreateNewsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW news AS
            (
                SELECT user.id AS user_id,
                user.name AS user_name,
                avatar.id_user AS id_us_avat,
                avatar.file_name,
                avatar.updated_at AS updated_avatar,
                posts.id_profile,
                posts.post,
                posts.updated_at AS updated_post,
                status.id_user AS id_us_stat,
                status.status,
                status.updated_at AS updated_status
               FROM ((('user'
                 JOIN avatar ON ((avatar.id_user = user.id)))
                 JOIN posts ON (((posts.id_user = user.id) AND (posts.id_profile = user.id))))
                 JOIN status ON ((status.id_user = user.id)));
            )"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS news_view');
    }
}
