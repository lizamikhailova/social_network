<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvatarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avatar', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');
            $table->string('file_name', 255);
            $table->boolean('is_active')->default(true);
            $table->timestampsTz();

            $table->softDeletesTz();
            $table->foreign('id_user')->references('id')->on('user')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('avatar', function (Blueprint $table) {
            $table->dropForeign(['id_user']);
        });

        Schema::drop('avatar');
    }
}