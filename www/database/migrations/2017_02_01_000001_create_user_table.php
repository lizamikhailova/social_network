<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 20);
            $table->string('email', 45)->unique();
            $table->string('password', 200)->unique();
            $table->boolean('visible')->default(true);
            $table->date('birthday')->nullable();
            $table->boolean('birthday_vis')->default(true);
            $table->boolean('agreement')->default(true);
            $table->timestampsTz();
            $table->rememberToken();

            $table->softDeletesTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user');
    }
}