<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');
            $table->integer('id_profile');
            $table->text('post');
            $table->timestampsTz();

            $table->softDeletesTz();
            $table->foreign('id_user')->references('id')->on('user')->onDelete('cascade');
            $table->foreign('id_profile')->references('id')->on('user')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropForeign(['id_user']);
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->dropForeign(['id_profile']);
        });

        Schema::drop('posts');
    }
}