<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeoContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geo_contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');
            $table->integer('id_country');
            $table->integer('id_city');
            $table->timestampsTz();

            $table->foreign('id_country')->references('id')->on('country')->onDelete('no action');
            $table->foreign('id_city')->references('id')->on('city')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('geo_contacts', function (Blueprint $table) {
            $table->dropForeign(['id_country']);
            $table->dropForeign(['id_city']);
        });

        Schema::drop('geo_contacts');
    }
}