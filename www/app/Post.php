<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    protected $table='posts';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $fillable=['post',
    'id_profile'];

    public function user()
    {
        return $this->belongsTo('App\User','id_user');
    }

    public function profile()
    {
        return $this->belongsTo('App\User','id_profile');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment','id_post');
    }

    public function likes()
    {
        return $this->belongsToMany('App\User','likes','id_post','id_user')->withPivot('id');
    }
}
