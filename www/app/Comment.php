<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table='comments';

    protected $fillable=['comment','id_user'];

    public function post()
    {
        return $this->belongsTo('App\Post','id_post');
    }

    public function user()
    {
        return $this->belongsTo('App\User','id_user');
    }
}
