<?php

namespace App;
use Illuminate\Contracts\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\NewPostNotification;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table='user';

    protected $fillable=[
        'name','email','password','visible'];

//    $user->notify(new NewPostNotification($invoice));

    public function posts()
    {
        return $this->hasMany('App\Post','id_user');
    }

    public function foreignPosts()
    {
        return $this->hasMany('App\Post','id_profile');
    }

    public function status(){
        return $this->hasMany('App\Status','id_user');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment','id_user');
    }

    public function avatars()
    {
        return $this->hasMany('App\Avatar','id_user');
    }

    public function lastname()
    {
        return $this->hasMany('App\Contact','id_user')->where('id_type',1);
    }

    public function afterfather()
    {
        return $this->hasMany('App\Contact','id_user')->where('id_type',2);
    }

    public function skype()
    {
        return $this->hasMany('App\Contact','id_user')->where('id_type',3);
    }

    public function phone()
    {
        return $this->hasMany('App\Contact','id_user')->where('id_type',4);
    }

    public function gender()
    {
        return $this->hasMany('App\Contact','id_user')->where('id_type',5);
    }

    public function subscribers()
    {
        return $this->belongsToMany('App\User','subscriptions','id_subscription','id_subscriber')->withPivot('id');
    }

    public function subscriptions()
    {
        return $this->belongsToMany('App\User','subscriptions','id_subscriber','id_subscription')->withPivot('id');
    }

    public function likes()
    {
        return $this->belongsToMany('App\Post','likes','id_post','id_user')->withPivot('id');
    }

    public function types()
    {
        return $this->belongsToMany('App\Types','contacts','id_user','id_type')->withPivot('id','type_value','visible');
    }

    public function cities()
    {
        return $this->belongsToMany('App\City')->using('App\GeoContacts');
    }

    public function countries()
    {
        return $this->belongsToMany('App\Country')->using('App\GeoContacts');
    }
}
