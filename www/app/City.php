<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{

    protected $table='city';

    public function country(){
        return $this->belongsToMany('App\Country','id_country');
    }

    public function users()
    {
        return $this->hasManyThrough('App\User','App\GeoContact');
    }
}
