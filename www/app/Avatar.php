<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Avatar extends Model
{
    use SoftDeletes;

    protected $table='avatar';

    protected $fillable=['file_name'];

    public function user()
    {
        return $this->belongsTo('App\User','id_user');
    }
}
