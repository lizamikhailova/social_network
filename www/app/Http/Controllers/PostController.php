<?php

namespace App\Http\Controllers;

use App\Notifications\NewPostNotification;
use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\Http\Controllers\UserController;

class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addPost(Request $request,User $user)
    {
        if($user->agreement==true){
            $user1 = \Auth::user();
            $post = $user1->posts()->create(['post'=>$request->post,
                'id_profile'=>$user->id]);
            if(\Auth::user()->id!=$user->id)
            {
                $user->notify(new NewPostNotification($post));
            }
            return back();
        }
        else {
            abort(403, 'Unauthorized action.');
        }
    }

    public function updatePost(Request $request, Post $post)
    {
        $user = \Auth::user();
        if($post->id_user==$user->id) {
            $post->update(['post'=>$request->post]);
            return back();
            }
        else {
            abort(403, 'Unauthorized action.');
        }
    }

    public function deletePost(Post $post)
    {
        if($post->id_user==\Auth::user()->id || $post->id_profile==\Auth::user()->id) {
            $post->delete();
            return back();
        }
        else {
            abort(403, 'Unauthorized action.');
        }
    }

    public function showDeleted(User $user)
    {
        if(\Auth::user()->id==$user->id){
            $posts = Post::onlyTrashed()->where('id_profile',$user->id)->get();
            return view('post.deletedPosts', ['user' => $user, 'posts' => $posts]);
        }
        else {
            abort(403, 'Unauthorized action.');
        }
    }

    public function restorePost(Post $post)
    {
        if(\Auth::user()->id==$post->id_profile){
            Post::onlyTrashed()->where('id',$post->id)->restore();
            return redirect()->route('user.profile',\Auth::user()->id);
        }
        else {
            abort(403, 'Unauthorized action.');
        }
    }

    public function finalDelete(Post $post)
    {
        if(\Auth::user()->id==$post->id_profile){
            Post::onlyTrashed()->where('id',$post->id)->forceDelete();
            return back();
        }
        else {
            abort(403, 'Unauthorized action.');
        }
    }
}
