<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Post;
use App\Status;
use App\Avatar;
use App\News;

class NavbarController extends Controller
{
    public function search(User $user)
    {
        return view('/search',['user'=>$user]);
    }

    public function subs(User $user){
        $subs = $user->find($user->id)->subscriptions()->get();
        $subid = $subs -> pluck('id');
        $subid -> toArray();
        return $subid;
    }

    public function showNews(User $user)
    {
        $subs = $this->subs($user);
        $news1 = News::whereIn('user_id',$subs)->get();
        $type = 'all';
        $coll = collect([]);
        foreach($news1 as $each) {
            $coll->push($each["original"]);
        }
//        dd($coll);
        $news = collect([]);
        foreach($coll as $el) {
            $post = Post::withTrashed()->where('id',$el["id_prop"])->first();
            $avatar = Avatar::withTrashed()->where('id',$el["id_prop"])->first();
            if(is_null($post)!=true && $post->post==$el["property"]){
                $news -> push(array_merge($el,["file_name" => Avatar::where('is_active',true)->where('id_user',$el["user_id"])->first()->file_name,"type"=>'post']));
            }
            elseif (is_null($avatar)!=true && $avatar->file_name==$el["property"]){
                $news -> push(array_merge($el,["file_name" => Avatar::where('is_active',true)->where('id_user',$el["user_id"])->first()->file_name, "type"=>'avatar']));
            }
            else {
                $news -> push(array_merge($el,["file_name" => Avatar::where('is_active',true)->where('id_user',$el["user_id"])->first()->file_name, "type"=>'status']));
            }
        }
        //dd(Avatar::where('is_active',true)->where('id_user',2)->first()->file_name);
        return view('news',['news' => $news, 'user' => $user, 'type' => $type]);
    }
//ready
    public function showPosts(User $user)
    {
        $subs = $this->subs($user);
        $news = Post::whereIn('id_profile',$subs)->orderBy('updated_at','desc')->get();
        $type = 'posts';
        return view('news',['news' => $news, 'user' => $user, 'type' => $type]);
    }
//ready
    public function showAvatars(User $user)
    {
        $subs =  $this->subs($user);
        $news = Avatar::whereIn('id_user',$subs)->orderBy('updated_at','desc')->get();
        $type = 'avatars';
        return view('news',['news' => $news, 'user' => $user, 'type' => $type]);
    }
//ready
    public function showStatus(User $user)
    {
        $subs =  $this->subs($user);
        $news = Status::whereIn('id_user',$subs)->orderBy('updated_at','desc')->get();
        $type = 'status';
        return view('news',['news' => $news, 'user' => $user, 'type' => $type]);
    }
}
