<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Status;

class StatusController extends Controller
{
    public function addStatus(Request $request,User $user)
    {
        if(\Auth::user()->id==$user->id) {
            $status = $user->status()->create(['status' => $request->status]);
        }
        else {
            abort(403,'Unauthorized action.');
        }
        return back();
    }

    public function updateStatus(Request $request, Status $status)
    {
        $user = \Auth::user();
        if($status->id_user==$user->id) {
            $status = $user->status()->create(['status' => $request->status]);
            return back();
        }
        else {
            abort(403,'Unauthorized action.');
        }
    }

    public function deleteStatus(Status $status)
    {
        if(\Auth::user()->id==$status->id_user){
            $status->delete();
            return back();
        }
        else {
            abort(403,'Unauthorized action.');
        }
    }
}
