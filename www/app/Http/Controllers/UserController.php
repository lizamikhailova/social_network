<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Types;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(User $user)
    {
        $me = \Auth::user();
        $last_name = $user->lastname()->first();
        $after_father = $user->afterfather()->first();
        $phone_number = $user->phone()->first();
        $skype = $user->skype()->first();
        $bday = $user->find($user->id)->birthday;
        $bdayvis = $user->find($user->id)->birthday_vis;
        $gender = $user->gender()->first();
        $agreement = $user->find($user->id)->agreement;
        $avatar = $user->avatars()->where('is_active', true)->first();
        if(is_null($avatar)){
            $user->avatars()->create(['file_name'=>'avatars/default.jpeg','is_active' => true]);
            $avatar = $user->avatars()->where('is_active', true)->first();
        }

        $status = $user->status()->orderBy('created_at','desc')->first();
        $posts = $user->find($user->id)->foreignPosts()->orderBy('created_at','desc')->get();
        $subscribers = $user->find($user->id)->subscribers()->get();
        $subscriptions = $user->find($user->id)->subscriptions()->get();

        if($subscribers->count()>5){
            $subscribers = $subscribers->random(6);
        }
        if($subscriptions->count()>5){
            $subscriptions = $subscriptions->random(6);
        }
        if($me!=$user){
            $subscript = $me->find($me->id)->subscriptions()->where('id_subscription',$user->id)->get();
            return view('profile', ['user' => $user, 'me'=>$me,'subscript' => $subscript,
                'posts' => $posts, 'status' => $status, 'avatar' => $avatar, 'subscribers' => $subscribers, 'subscriptions' => $subscriptions,
                'last_name' => $last_name, 'after_father' => $after_father,
                'phone_number' => $phone_number, 'skype' => $skype,'bday' => $bday, 'bdayvis' => $bdayvis, 'gender' => $gender,
                'agreement' => $agreement]);
        }
        return view('profile', ['user' => $user, 'me'=>$me,
                'posts' => $posts, 'status' => $status, 'avatar' => $avatar, 'subscribers' => $subscribers, 'subscriptions' => $subscriptions,
                'last_name' => $last_name, 'after_father' => $after_father,
                'phone_number' => $phone_number, 'skype' => $skype,'bday' => $bday, 'bdayvis' => $bdayvis, 'gender' => $gender,
            'agreement' => $agreement]);
    }
}