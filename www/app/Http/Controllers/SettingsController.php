<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Types;
use App\User;
use phpDocumentor\Reflection\Types\String_;
use Carbon\Carbon;


class SettingsController extends Controller
{
    public function showSettings(User $user)
    {
        if (\Auth::user()->id==$user->id) {
        $last_name = $user->lastname()->first();
        $after_father = $user->afterfather()->first();
        $phone_number = $user->phone()->first();
        $skype = $user->skype()->first();
        $gender = $user->gender()->first();
        $visible = $user->find($user->id)->visible;
        $bday = $user->find($user->id)->birthday;
        $bdayvis = $user->find($user->id)->birthday_vis;
        $agreement = $user->find($user->id)->agreement;
        return view('settings', ['user' => $user,'last_name' => $last_name,
                'phone_number' => $phone_number,
                'after_father' => $after_father,
                'skype' => $skype, 'gender' => $gender, 'visible'=>$visible, 'bday' => $bday, 'bdayvis' => $bdayvis, 'agreement' => $agreement]);
    }
    else
        return redirect()->route('user.settings',\Auth::user()->id);
    }

    public function settings(Request $request, User $user)
    {
        $last_name = $user->lastname()->first();
        $after_father = $user->afterfather()->first();
        $phone_number = $user->phone()->first();
        $skype = $user->skype()->first();
        $gender = $user->gender()->first();
        $bday = $user->find($user->id);
        $bdayvis = $user->find($user->id)->birthday_vis;
        $agreement = $user->find($user->id)->agreement;

        $id_l=Types::where('type_name','last_name')->firstOrFail()->id;
        $id_f=Types::where('type_name','after_father')->firstOrFail()->id;
        $id_ph=Types::where('type_name','phone_number')->firstOrFail()->id;
        $id_sk=Types::where('type_name','skype')->firstOrFail()->id;
        $id_g=Types::where('type_name','gender')->firstOrFail()->id;

        //visibility
        if(is_null($request->visible1)){
            $request->visible1=false;
        } else {
            $request->visible1=true;}

        if (is_null($request->visible2)) {
            $request->visible2 = false;
        } else {
            $request->visible2=true;}

        if (is_null($request->visible3)) {
            $request->visible3 = false;
        } else {
            $request->visible3 = true;}

        if (is_null($request->visible4)) {
            $request->visible4 = false;
        } else {
            $request->visible4 = true;}

        if (is_null($request->bdayvis)) {
            $request->bdayvis = false;
        } else {
            $request->bdayvis = true;}

        if (is_null($request->netVisibility)) {
            $request->netVisibility = false;
        } else {
            $request->netVisibility = true;}

        if (is_null($request->agreement)) {
            $request->agreement = false;
        } else {
            $request->agreement = true;}

        //validating data
        //last name
        if($request->last_name!=""){
            if(is_null($last_name)){
                $last_name = $user->types()->save($user,['id_type' => $id_l,'type_value' => $request->last_name,'visible'=>$request->visible1]);
            } else {
                $last_name->type_value=$request->last_name;
                $last_name->visible=$request->visible1;
                $last_name->save();
            }
        }

        //name after father
        if ($request->after_father != "") {
            if (is_null($after_father)) {
                $after_father = $user->types()->save($user, ['id_type' => $id_f, 'type_value' => $request->after_father, 'visible' => $request->visible2]);
            } else {
                $after_father->type_value = $request->after_father;
                $after_father->visible = $request->visible2;
                $after_father->save();
            }
        }

        //phone mumber
        if ($request->phone_number != "") {
            if(is_null($phone_number)) {
                $phone_number = $user->types()->save($user, ['id_type' => $id_ph, 'type_value' => $request->phone_number, 'visible' => $request->visible4]);
            } else {
                $phone_number->type_value=$request->phone_number;
                $phone_number->visible=$request->visible4;
                $phone_number->save();
            }
        }

        //skype
        if ($request->skype != "") {
            if(is_null($skype)) {
                $skype = $user->types()->save($user, ['id_type' => $id_sk, 'type_value' => $request->skype, 'visible' => $request->visible3]);
            } else {
                $skype->type_value=$request->skype;
                $skype->visible=$request->visible3;
                $skype->save();
            }
        }

        if(is_null($gender)){
            $gender = $user->types()->save($user, ['id_type' => $id_g, 'type_value' => $request->gender, 'visible' => false]);
        }

//        $now=Carbon::parse(Carbon::now())->format('d/m/Y');
//        $dob=Carbon::parse($request->bday)->format('d/m/Y');
        $bday->birthday = $request->bday;
        $bday->save();
        $temp = $user->find($user->id);
        $temp->birthday_vis = $request->bdayvis;
        $temp->save();
//        if ($dob>$now) {
//
//        } else{
//            return redirect()->route('user.settings', $user->id);
//        }

        $agree= User::find($user->id);
        $agree->agreement = $request->agreement;
        $agree->save();

        $vis = User::find($user->id);
        $vis->visible = $request->netVisibility;
        $vis->save();

        return redirect()->route('user.profile', $user->id);
    }
}
