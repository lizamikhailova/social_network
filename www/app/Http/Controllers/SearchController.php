<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;

class SearchController extends Controller
{
    public function search(Request $request, User $user)
    {
        $now = Carbon::now();
        $fromage=$request->fromage;
        settype($fromage,"integer");
        $toage=$request->toage;
        settype($toage,"integer");
        if($toage==0){
            $toage=89;
        }
        if($request->gender!="Gender"){
            if($request->last_name=="" && $request->name==""){
                $users=User::where('visible',true)
                    ->where('birthday','<',Carbon::parse($now->subYears($fromage))->format('Y-m-d'))
                    ->where('birthday','>',Carbon::parse($now->subYears($toage))->format('Y-m-d'))
                    ->whereHas('types', function($query) use ($request){
                    $query -> where([
                        ['id_type',5],
                        ['type_value',strtolower($request->gender)]]);
                })->get();
            }
            elseif($request->last_name!="" && $request->name=="") {
                $users=User::where('visible',true)
                    ->where('birthday','<',Carbon::parse($now->subYears($fromage))->format('Y-m-d'))
                    ->where('birthday','>',Carbon::parse($now->subYears($toage))->format('Y-m-d'))
                ->whereHas('types', function($query) use ($request){
                    $query ->
                    where([
                        ['id_type',5],
                        ['type_value',strtolower($request->gender)]]);
                })->whereHas('types', function($query) use ($request){
                    $query ->
                    where([
                        ['id_type',1],
                        ['type_value','like','%'.$request->last_name.'%']]);
                })->get();
            }
            elseif ($request->last_name=="" && $request->name!="") {
                $users=User::where('visible',true)->where('name','like','%'.$request->name.'%')
                    ->where('birthday','<',Carbon::parse($now->subYears($fromage))->format('Y-m-d'))
                    ->where('birthday','>',Carbon::parse($now->subYears($toage))->format('Y-m-d'))
                    ->whereHas('types', function($query) use ($request){
                    $query -> where([
                        ['id_type',5],
                        ['type_value',strtolower($request->gender)]]);
                })->get();
            }
            elseif ($request->last_name!="" && $request->name!="") {
                $users=User::where('visible',true)->where('name','like','%'.$request->name.'%')
                    ->where('birthday','<',Carbon::parse($now->subYears($fromage))->format('Y-m-d'))
                    ->where('birthday','>',Carbon::parse($now->subYears($toage))->format('Y-m-d'))
                    ->whereHas('types', function($query) use ($request){
                    $query ->
                    where([
                        ['id_type',5],
                        ['type_value',strtolower($request->gender)]]);
                })->whereHas('types', function($query) use ($request){
                    $query ->
                    where([
                        ['id_type',1],
                        ['type_value','like','%'.$request->last_name.'%']]);
                })->get();
            }
        } elseif($request->gender=="Gender") {
            if($request->last_name=="" && $request->name==""){
                $users=User::where('visible',true)
                    ->where('birthday','<',Carbon::parse($now->subYears($fromage))->format('Y-m-d'))
                    ->where('birthday','>',Carbon::parse($now->subYears($toage))->format('Y-m-d'))
                    ->get();
            }
            elseif($request->last_name=="" && $request->name!=""){
                $users=User::where('visible',true)->where('name','like','%'.$request->name.'%')
                    ->where('birthday','<',Carbon::parse($now->subYears($fromage))->format('Y-m-d'))
                    ->where('birthday','>',Carbon::parse($now->subYears($toage))->format('Y-m-d'))
                    ->get();
            }
            elseif($request->last_name!="" && $request->name==""){
                $users=User::where('visible',true)
                    ->where('birthday','<',Carbon::parse($now->subYears($fromage))->format('Y-m-d'))
                    ->where('birthday','>',Carbon::parse($now->subYears($toage))->format('Y-m-d'))
                    ->whereHas('types', function($query) use ($request){
                        $query ->
                        where([
                            ['id_type',1],
                            ['type_value','like','%'.$request->last_name.'%']]);
                    })->get();
            }
            elseif ($request->last_name!="" && $request->name!="") {
                $users=User::where('visible',true)->where('name','like','%'.$request->name.'%')
                    ->where('birthday','<',Carbon::parse($now->subYears($fromage))->format('Y-m-d'))
                    ->where('birthday','>',Carbon::parse($now->subYears($toage))->format('Y-m-d'))
                   ->whereHas('types', function($query) use ($request){
                        $query ->
                        where([
                            ['id_type',1],
                            ['type_value','like','%'.$request->last_name.'%']]);
                    })->get();
            }
        }
//        dd($users);
        return view('search',['user' => $user,'users' => $users]);
    }
}
