<?php

namespace App\Http\Controllers;

use App\Notifications\NewCommentNotification;
use Illuminate\Http\Request;
use App\User;
use App\Post;
use App\Comment;

class CommentController extends Controller
{
    public function leaveComment(Post $post, Request $request)
    {
        $comment = $post->comments()->create(['comment' => $request->comment,
        'id_user' => \Auth::user()->id]);
        if(\Auth::user()->id!=$post->id_user)
        {
            $user=User::where('id',$post->id_user)->first();
            $user->notify(new NewCommentNotification($comment));
        }
        if(\Auth::user()->id!=$post->id_profile && $post->id_user!=$post->id_profile)
        {
            $user2=User::where('id',$post->id_profile)->first();
            $user2->notify(new NewCommentNotification($comment));
        }
        return back();
    }

    public function updateComment(Request $request, Comment $comment)
    {
        $user = \Auth::user()->id;
        if($user==$comment->id_user){
            $comment -> update(['comment' => $request->comment]);
            return back();
        }
        else{
            abort(403,'Unauthorized action.');
        }
    }

    public function deleteComment(Comment $comment)
    {
        $user = \Auth::user()->id;
        $profile = $comment->post->id_profile;
        if($user==$comment->id_user || $user==$profile) {
            $comment->delete();
            return back();
        }
        else{
            abort(403,'Unauthorized action.');
        }
    }
}
