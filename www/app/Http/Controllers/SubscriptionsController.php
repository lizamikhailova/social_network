<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;

class SubscriptionsController extends Controller
{
    public function subscribe(User $user)
    {
        $me=\Auth::user();
        $me->subscriptions()->save($me,['id_subscriber' => $me->id,'id_subscription' => $user->id]);
        return back();
    }

    public function unSubscribe(User $user)
    {
        $me=\Auth::user();
        $me->subscriptions()->detach($user->id);
        return back();
    }

    public function showSubscribers(User $user){
        $me=\Auth::user();
        $subscribers = $user->find($user->id)->subscribers()->get();
        return view('subscribers',['user' => $user, 'me' => $me, 'subscribers' => $subscribers]);
    }

    public function showSubscriptions(User $user){
        $me=\Auth::user();
        $subscriptions = $user->find($user->id)->subscriptions()->get();
        return view('subscriptions',['user' => $user, 'me' => $me, 'subscriptions' => $subscriptions]);
    }

    public function searchSubscribers(User $user, Request $request, array $subsid, $type)
    {
        $now = Carbon::now();
        $fromage=$request->fromage;
        settype($fromage,"integer");
        $toage=$request->toage;
        settype($toage,"integer");
        if($toage==0){
            $toage=89;
        }
        if($request->gender!="Gender"){
            if($request->last_name=="" && $request->name==""){
                $subs=User::whereIn('id',$subsid)->where('visible',true)
                    ->where('birthday','<',Carbon::parse($now->subYears($fromage))->format('Y-m-d'))
                    ->where('birthday','>',Carbon::parse($now->subYears($toage))->format('Y-m-d'))
                    ->whereHas('types', function($query) use ($request){
                        $query -> where([
                            ['id_type',5],
                            ['type_value',strtolower($request->gender)]]);
                    })->get();
            }
            elseif($request->last_name!="" && $request->name=="") {
                $subs=User::whereIn('id',$subsid)->where('visible',true)
                    ->where('birthday','<',Carbon::parse($now->subYears($fromage))->format('Y-m-d'))
                    ->where('birthday','>',Carbon::parse($now->subYears($toage))->format('Y-m-d'))
                    ->whereHas('types', function($query) use ($request){
                        $query ->
                        where([
                            ['id_type',5],
                            ['type_value',strtolower($request->gender)]]);
                    })->whereHas('types', function($query) use ($request){
                        $query ->
                        where([
                            ['id_type',1],
                            ['type_value','like','%'.$request->last_name.'%']]);
                    })->get();
            }
            elseif ($request->last_name=="" && $request->name!="") {
                $subs=User::whereIn('id',$subsid)->where('visible',true)->where('name','like','%'.$request->name.'%')
                    ->where('birthday','<',Carbon::parse($now->subYears($fromage))->format('Y-m-d'))
                    ->where('birthday','>',Carbon::parse($now->subYears($toage))->format('Y-m-d'))
                    ->whereHas('types', function($query) use ($request){
                        $query -> where([
                            ['id_type',5],
                            ['type_value',strtolower($request->gender)]]);
                    })->get();
            }
            elseif ($request->last_name!="" && $request->name!="") {
                $subs=User::whereIn('id',$subsid)->where('visible',true)->where('name','like','%'.$request->name.'%')
                    ->where('birthday','<',Carbon::parse($now->subYears($fromage))->format('Y-m-d'))
                    ->where('birthday','>',Carbon::parse($now->subYears($toage))->format('Y-m-d'))
                    ->whereHas('types', function($query) use ($request){
                        $query ->
                        where([
                            ['id_type',5],
                            ['type_value',strtolower($request->gender)]]);
                    })->whereHas('types', function($query) use ($request){
                        $query ->
                        where([
                            ['id_type',1],
                            ['type_value','like','%'.$request->last_name.'%']]);
                    })->get();
            }
        } elseif($request->gender=="Gender") {
            if($request->last_name=="" && $request->name==""){
                $subs=User::whereIn('id',$subsid)->where('visible',true)
                    ->where('birthday','<',Carbon::parse($now->subYears($fromage))->format('Y-m-d'))
                    ->where('birthday','>',Carbon::parse($now->subYears($toage))->format('Y-m-d'))
                    ->get();
            }
            elseif($request->last_name=="" && $request->name!=""){
                $subs=User::whereIn('id',$subsid)->where('visible',true)->where('name','like','%'.$request->name.'%')
                    ->where('birthday','<',Carbon::parse($now->subYears($fromage))->format('Y-m-d'))
                    ->where('birthday','>',Carbon::parse($now->subYears($toage))->format('Y-m-d'))
                    ->get();
            }
            elseif($request->last_name!="" && $request->name==""){
                $subs=User::whereIn('id',$subsid)->where('visible',true)
                    ->where('birthday','<',Carbon::parse($now->subYears($fromage))->format('Y-m-d'))
                    ->where('birthday','>',Carbon::parse($now->subYears($toage))->format('Y-m-d'))
                    ->whereHas('types', function($query) use ($request){
                        $query ->
                        where([
                            ['id_type',1],
                            ['type_value','like','%'.$request->last_name.'%']]);
                    })->get();
            }
            elseif ($request->last_name!="" && $request->name!="") {
                $subs=User::whereIn('id',$subsid)->where('visible',true)->where('name','like','%'.$request->name.'%')
                    ->where('birthday','<',Carbon::parse($now->subYears($fromage))->format('Y-m-d'))
                    ->where('birthday','>',Carbon::parse($now->subYears($toage))->format('Y-m-d'))
                    ->whereHas('types', function($query) use ($request){
                        $query ->
                        where([
                            ['id_type',1],
                            ['type_value','like','%'.$request->last_name.'%']]);
                    })->get();
            }
        }
        $me = \Auth::user();

        if($type == 'subscribers'){
            $subscribers = $subs;
            return view('subscribers',['user' => $user, 'me' => $me,'subscribers' => $subscribers]);
        } elseif($type == 'subscriptions'){
            $subscriptions = $subs;
            return view('subscriptions',['user' => $user, 'me' => $me,'subscriptions' => $subscriptions]);
        }
    }

    public function subscriptions(User $user, Request $request){
        $subs = $user->find($user->id)->subscriptions()->get();
        $subsid = $subs -> pluck('id')-> toArray();
        $type = 'subscriptions';
        return $this->searchSubscribers($user,$request, $subsid, $type);
    }

    public function subscribers(User $user, Request $request){
        $subs = $user->find($user->id)->subscribers()->get();
        $subsid = $subs -> pluck('id')-> toArray();
        $type = 'subscribers';
        return $this->searchSubscribers($user, $request, $subsid, $type);
    }
}
