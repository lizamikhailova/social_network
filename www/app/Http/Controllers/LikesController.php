<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Post;

class LikesController extends Controller
{
    public function like(Post $post)
    {
        $me = \Auth::user();
        $me->likes()->save($me,['id_user' => $me->id, 'id_post' => $post->id]);
        return back();
    }

    public function dislike(Post $post)
    {
        $me = \Auth::user();
        $post->likes()->detach($me->id);
        return back();
    }
}
