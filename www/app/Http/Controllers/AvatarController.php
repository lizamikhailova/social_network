<?php

namespace App\Http\Controllers;

use App\User;
use App\Avatar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AvatarController extends Controller
{
    public function store(Request $request)
    {
        $user = \Auth::user();
        $user->avatars()->where('is_active',true)->update(['is_active' => false]);
        $avatars = $user->avatars()->first();
        $path = request()->file('avatar')->store('avatars', 'public');
        if(count($avatars)==1 && $avatars->file_name=="avatars/default.jpeg"){
            $avatars->file_name = $path;
            $avatars->is_active = true;
            $avatars->save();
        } else {
            $avatar = $user->avatars()->create(['file_name' => $path, 'is_active' => true]);
        }
        return back();
    }

    public function updateAvatar(Request $request, User $user){
        if(\Auth::user()->id!=$user->id){
            return back();
        } else{
            $current = $user->avatars()->where('is_active',true)->first();
            $current->is_active = false;
            $current->save();
            $new = $user->avatars()->where('id',$request->avatar)->first();
            $new->is_active = true;
            $new->save();
            return back();
        }
    }

    public function deleteAvatar(Request $request, User $user){
        if(\Auth::user()->id!=$user->id){
            return back();
        } else {
            $avatar = $user->avatars()->where('id', $request->avatar)->first();
            $avatar->delete();
            return back();
        }
    }
}