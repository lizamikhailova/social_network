<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function main() {
        if(\Auth::user()){
            $user = \Auth::user();
            return view('layouts.app',['user' => $user]);
        } else {
            return view('layouts.app');
        }
    }

    public function index()
    {
        $user = \Auth::user();
        return view('home', ['user' => $user]);
    }
}
