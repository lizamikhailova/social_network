<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Types extends Model
{

    protected $table='types';

    public function users()
    {
        return $this->belongsToMany('App\User','contacts','id_type','id_user')->withPivot('id','type_value','visible');
    }
}
