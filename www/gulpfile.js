const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

elixir(function(mix){
    mix.sass('app.scss');
});

elixir(function(mix){
    mix.styles([
        'mystyles.css',
        './bower_components/bootstrap/dist/css/bootstrap.css',
        './bower_components/bootstrap/dist/css/bootstrap.css.map',
        './bower_components/cropperjs/dist/cropper.css'
    ], './public/css/app.css')

    .scripts([
        './bower_components/jquery/dist/jquery.min.js',
        './bower_components/bootstrap/dist/js/bootstrap.min.js',
        './bower_components/cropperjs/dist/cropper.js'
    ], './public/js/app.js')

    .copy('bower_components/bootstrap/fonts/','public/fonts');
});

