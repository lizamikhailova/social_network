# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrant LEMP Ansible
# A PHP7 development platform in a box.
# See the readme file (README.md) for more information.

# If vagrant-trigger isn't instaled then exit
if !Vagrant.has_plugin?("vagrant-triggers")
  puts "'vagrant-triggers' plugin is required"
  puts "This can be installed by running:"
  puts
  puts " vagrant plugin install vagrant-triggers"
  puts
  exit
end

# Find the current vagrant directory.
vagrant_dir = File.expand_path(File.dirname(__FILE__))


# Include config from settings.yml
require 'yaml'
vconfig = YAML::load_file(vagrant_dir + "/playbooks/settings.yml")
provision_hosts_file = vconfig['provisions_dir'] + '/host.ini'

# Configuration
boxipaddress = vconfig['boxipaddress']
boxname = vconfig['boxname']

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  # Configure virtual machine options.
  config.vm.box = "ubuntu/trusty64"
  config.vm.box_url = "https://atlas.hashicorp.com/ubuntu/trusty64"
  config.vm.hostname = boxname

  config.vm.network :private_network, ip: boxipaddress
  #config.vm.network "forwarded_port", guest: 5432, host: 15432

  # Set *Vagrant* VM name
  config.vm.define boxname do |boxname|
  end

  # Configure virtual machine setup.
  config.vm.provider :virtualbox do |v|
    v.customize ["modifyvm", :id, "--memory", vconfig["memory"]]
    v.customize ["modifyvm", :id, "--cpus", vconfig["cpus"]]
    v.customize ["modifyvm", :id, "--ioapic", "on"]
    v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
  end

  # Set up NFS drive.
  nfs_setting = RUBY_PLATFORM =~ /darwin/ || RUBY_PLATFORM =~ /linux/

  # SSH Set up.
  config.ssh.forward_agent = true



  # Run the halt/destroy playbook upon halting or destroying the box
  if File.exist?(provision_hosts_file)
    config.trigger.before [:halt, :destroy], :stdout => true, :force => true do
      run_remote 'ansible-playbook -i ' + boxipaddress + ', --ask-sudo-pass ' + vconfig['provisions_dir'] + '/local_halt_destroy.yml'
    end
  end

  provisioner = Vagrant::Util::Platform.windows? ? :ansible_local : :ansible

  # Provision vagrant box with Ansible.
  config.vm.provision provisioner do |ansible|
    ansible.playbook = "playbooks/setup.yml"
    #ansible.host_key_checking = false
    ansible.extra_vars = {user:"vagrant"}
    if vconfig['ansible_verbosity'] != ''
      ansible.verbose = vconfig['ansible_verbosity']
    end
  end

  # Create synced folders for each virtual host.
  vconfig['vhosts'].each do |vhost|
    site_alias = vhost['alias']
    folder_path = vhost['path']
    config.vm.synced_folder folder_path,
      vconfig['sites_dir'] + "/" + site_alias,
      create: true,
      type: "nfs",
      :nfs => nfs_setting
  end

  config.vm.synced_folder vagrant_dir + '/playbooks',
        vconfig['provisions_dir'],
        create: true,
        type: "nfs",
        :nfs => nfs_setting

  # Create all virtual hosts
  config.trigger.after [:up, :reload], :stdout => true, :force => true do
        # Run an Ansible playbook on setting the box up
          if !File.exist?(provision_hosts_file)
            #config.trigger.before :up, :stdout => true, :force => true do
              run_remote 'ansible-playbook -i ' + boxipaddress + ', ' + vconfig['provisions_dir'] + '/local_up.yml --extra-vars "local_ip_address=' + boxipaddress + '"'
            #end
          end

    run_remote "ansible-playbook -i " + vconfig['home_dir'] + "/host.ini " + vconfig['provisions_dir'] + "/virtual_hosts.yml"

  end

end
